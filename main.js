const commandLineArgs = require('command-line-args')
const path = require('path')
const url = require('url')
const {
    app,
    globalShortcut,
    BrowserWindow
} = require('electron')


let mainWindow

const optionDefinitions = [
    { name: 'dev', alias: 'd', type: Boolean, defaultValue: false },
    { name: 'url', alias: 'u', type: String, defaultValue: null }
]
const options = commandLineArgs(optionDefinitions)


// ----------------------------------------------------------------------------

function createWindow()
{
    mainWindow = new BrowserWindow({
        width: 1920,
        height: 1080,
        kiosk: true,
        webPreferences: {
            nodeIntegration: true
        }
    })

    if(process.env.XOT_WEBWRAPPER_URL != null) {
        mainWindow.webContents.loadURL(process.env.XOT_WEBWRAPPER_URL)
    } else if(options.url != null) {
        mainWindow.webContents.loadURL(options.url)
    } else {
        mainWindow.loadFile('error.html')
    }

    mainWindow.on('closed', function () {
        mainWindow = null
    })
}


// -- Event Handlers
// ----------------------------------------------------------------------------

app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
})

app.on('ready', function() {
    createWindow()

    globalShortcut.register('esc', function() {
        app.quit()
    })

    if(options.dev) {
        globalShortcut.register('CmdOrCtrl+D', function() {
            mainWindow.webContents.openDevTools()
        })
    }
})

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})
