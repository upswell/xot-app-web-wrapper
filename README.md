# Upswell Cloud, Web Application Wrapper

The Web Application Wrapper is a cross-platform kiosk container for Javascript Single Page Applications (SPA's).

It is part of the [Upswell Cloud](https://upswell.cloud) Common Application repository providing simple reusable tools for deploying on-site media experiences.


## Dependencies

The following tools are required for development. For installation instructions follow the links below:

* [Node.js](https://nodejs.org/en/download/package-manager/)
* [Yarn](https://yarnpkg.com/lang/en/docs/install/)


## Quick Start

The Web Application Wrapper uses [Yarn](https://yarnpkg.com/) for dependency management and as a script runner.

### Install Dependencies

```bash
yarn install
```

### Run for Development

```bash
yarn start --dev
```

To access the Chrome development console press `Cmd+D` or `Ctl+D`.

To exit the application `Esc` is enabled in `--dev` mode, otherwise the OS specific exit command such as `Cmd+Q` or `Alt-F4` can be used.


## Packaging

Application packaging is managed by [`electron-builder`](https://www.electron.build/).

To package a build:

```bash
yarn dist
```

Application bundles on all OS's are created as ZIP's for deployment through the Upswell Cloud.

Many additional configuration options exist and can be found on the [Common Configuration](https://www.electron.build/configuration/configuration) page for Electron Builder.

## Configuration

The Web Wrapper can be configured with either the environment variables or command line arguments.

While both configuration methods can be used simultaneously, it is recommended that one approach be selected to eliminate confusion. When a choice must be made between the two, environment variables take first priority and will fall back to command line arguments when not available.

### Environment Variables

| Variable              | Description   |
| --                    | --            |
| `XOT_WEBWRAPPER_URL`  | The full URL of the SPA to load at run-time.  |

Example

```bash
export XOT_WEBWRAPPER_URL=https://upswell.cloud
open Upswell\ Cloud\ Web\ Wrapper.app
```

### Command Line Arguments

| Argument      | Alias     | Description   |
| --            | --        | --            |
| `--dev`       | `-d`      | Enable Electron development features. |
| `--url`       | `-u`      | The full URL of the SPA to load at run-time. |

Example

```bash
open Upswell\ Cloud\ Web\ Wrapper.app --url=https://upswell.cloud
```

## About Upswell

[Upswell](https://www.hello-upswell.com) is an independent creative partner to forward-thinking leaders tackling the complex challenges facing our world today.

We combine storytelling with creative applications of technology to create experiences that stimulate different wats of seeing, understanding, and
engaging with the urgent topics of our time.


## About the Upswell Cloud

The [Upswell Cloud](https://upswell.cloud) provides organizations with the tools needed to manage the day-to-day development, deployment and operation of physical media spaces.

For more information contact Upswell's Director of Technology, [Kieran Lynn](mailto:kieran@hello-upswell.com).
